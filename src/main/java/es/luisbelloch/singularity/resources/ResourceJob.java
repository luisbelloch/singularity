package es.luisbelloch.singularity.resources;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ResourceJob {

    private final String[] tenants;

    private final ResourceProducer producer;

    public ResourceJob(ResourceProducer producer, @Value("${singularity.tenants}") String[] tenants) {
        this.tenants = tenants;
        this.producer = producer;
    }

    @Scheduled(fixedRate = 5000)
    public void run() {
        for (String tenant : tenants) {
            this.producer.produce(tenant);
        }
    }
}
