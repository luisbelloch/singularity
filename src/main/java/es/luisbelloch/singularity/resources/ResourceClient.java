package es.luisbelloch.singularity.resources;

import es.luisbelloch.singularity.shared.Ack;
import feign.Headers;
import feign.RequestLine;

public interface ResourceClient {

    @RequestLine("POST /{functionName}")
    @Headers("Content-Type: application/json")
    Ack post(Resource resource);
}
