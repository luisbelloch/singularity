package es.luisbelloch.singularity.resources;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {

    private final ResourceRepository repository;

    public ResourceController(ResourceRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/v1/{tenant}/resources")
    public Page<Resource> list(@PathVariable("tenant") String tenant, Pageable page) {
        return repository.findByTenant(tenant, page);
    }
}
