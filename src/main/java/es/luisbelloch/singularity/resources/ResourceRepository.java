package es.luisbelloch.singularity.resources;

import es.luisbelloch.singularity.shared.EntityId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface ResourceRepository extends PagingAndSortingRepository<Resource, EntityId> {

    Page<Resource> findByTenant(String tenant, Pageable pageable);

    Optional<Resource> findFirstByTenantAndExternalIdIsNullOrderByIdAsc(String tenant);

    Page<Resource> findAllByTenantAndExternalIdIsNotNull(String tenant, Pageable pageable);

    @Query(value = "select r.id from resource r where r.tenant = ?1 and r.external_id is not null order by rand() limit 1", nativeQuery = true)
    String findRandomEmittedResource(String tenant);

    @Modifying
    @Query("update Resource r set r.externalId = null where r.tenant = :tenant")
    void reset(String tenant);
}
