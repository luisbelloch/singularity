package es.luisbelloch.singularity.resources;

import es.luisbelloch.singularity.audit.AuditService;
import es.luisbelloch.singularity.client.ClientException;
import es.luisbelloch.singularity.client.ClientFactory;
import es.luisbelloch.singularity.endpoints.EndpointService;
import es.luisbelloch.singularity.shared.Ack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ResourceProducer {

    private static final Logger log = LoggerFactory.getLogger(ResourceProducer.class);

    private final EndpointService endpointService;
    private final ResourceRepository resourceRepository;
    private final ClientFactory clientFactory;
    private final AuditService auditService;

    public ResourceProducer(EndpointService endpointService, ResourceRepository resourceRepository, ClientFactory clientFactory, @Qualifier("resource") AuditService auditService) {
        this.endpointService = endpointService;
        this.resourceRepository = resourceRepository;
        this.clientFactory = clientFactory;
        this.auditService = auditService;
    }

    public void produce(String tenant) {
        endpointService.find(tenant, "resource").ifPresent(endpoint -> {
            if (!endpoint.isActive()) {
                return;
            }

            Optional<Resource> next = this.resourceRepository.findFirstByTenantAndExternalIdIsNullOrderByIdAsc(tenant);
            if (next.isEmpty()) {
                log.info("RESOURCE|{}||No more resources", tenant);
                this.endpointService.activate(tenant, "resource", false);
                return;
            }

            Resource resource = next.get();
            post(tenant, endpoint.getUrl(), resource);
        });
    }

    private void post(String tenant, String url, Resource resource) {
        try {
            ResourceClient client = this.clientFactory.resourceClient(url);
            Ack ack = client.post(resource);
            resource.setExternalId(ack.getExternalId());

            this.resourceRepository.save(resource);
            this.auditService.ok(tenant, ack);
        } catch (ClientException exception) {
            this.auditService.error(tenant, exception);
        } catch (Exception exception) {
            this.auditService.error(tenant, exception);
        }
    }
}
