package es.luisbelloch.singularity.reset;

import es.luisbelloch.singularity.audit.AuditRepository;
import es.luisbelloch.singularity.endpoints.EndpointRepository;
import es.luisbelloch.singularity.events.EventRepository;
import es.luisbelloch.singularity.resources.ResourceRepository;
import es.luisbelloch.singularity.users.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ResetService {
    private final AuditRepository auditRepository;
    private final EndpointRepository endpointRepository;
    private final EventRepository eventRepository;
    private final ResourceRepository resourceRepository;
    private final UserRepository userRepository;

    public ResetService(AuditRepository auditRepository, EndpointRepository endpointRepository,
                        EventRepository eventRepository, ResourceRepository resourceRepository,
                        UserRepository userRepository) {
        this.auditRepository = auditRepository;
        this.endpointRepository = endpointRepository;
        this.eventRepository = eventRepository;
        this.resourceRepository = resourceRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void reset(String tenant) {
        auditRepository.clear(tenant);
        eventRepository.clear(tenant);
        endpointRepository.disableAll(tenant);
        resourceRepository.reset(tenant);
        userRepository.reset(tenant);
    }
}
