package es.luisbelloch.singularity.reset;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResetController {

    private final ResetService resetService;

    public ResetController(ResetService resetService) {
        this.resetService = resetService;
    }

    @PostMapping("/v1/{tenant}/reset")
    public void list(@PathVariable("tenant") String tenant) {
        resetService.reset(tenant);
    }
}
