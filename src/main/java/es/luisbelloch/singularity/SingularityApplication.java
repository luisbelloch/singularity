package es.luisbelloch.singularity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SingularityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SingularityApplication.class, args);
    }

    @Bean
    public Jackson2RepositoryPopulatorFactoryBean getRepositoryPopulator() {
        Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();
        factory.setResources(new Resource[]{
                new ClassPathResource("data/endpoints.json"),
                new ClassPathResource("data/categories.json"),
                new ClassPathResource("data/users.json"),
                new ClassPathResource("data/resources.json")
        });
        return factory;
    }
}
