package es.luisbelloch.singularity.shared;

import java.io.Serializable;
import java.util.Objects;

public class EntityId implements Serializable {
    private String tenant;
    private String id;

    public EntityId() {
    }

    public EntityId(String tenant, String id) {
        this.tenant = tenant;
        this.id = id;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityId that = (EntityId) o;
        return tenant.equals(that.tenant) && id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tenant, id);
    }
}
