package es.luisbelloch.singularity.users;

import es.luisbelloch.singularity.audit.AuditService;
import es.luisbelloch.singularity.client.ClientException;
import es.luisbelloch.singularity.client.ClientFactory;
import es.luisbelloch.singularity.endpoints.EndpointService;
import es.luisbelloch.singularity.shared.Ack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserProducer {

    private static final Logger log = LoggerFactory.getLogger(UserProducer.class);

    private final EndpointService endpointService;
    private final UserRepository userRepository;
    private final ClientFactory clientFactory;
    private final AuditService auditService;

    public UserProducer(EndpointService endpointService, UserRepository userRepository, ClientFactory clientFactory, @Qualifier("user") AuditService auditService) {
        this.endpointService = endpointService;
        this.userRepository = userRepository;
        this.clientFactory = clientFactory;
        this.auditService = auditService;
    }

    public void produce(String tenant) {
        endpointService.find(tenant, "user").ifPresent(endpoint -> {
            if (!endpoint.isActive()) {
                return;
            }

            Optional<User> next = this.userRepository.findFirstByTenantAndExternalIdIsNullOrderByIdAsc(tenant);
            if (next.isEmpty()) {
                log.info("USER|{}|No more users", tenant);
                this.endpointService.activate(tenant, "user", false);
                return;
            }

            User user = next.get();
            post(tenant, endpoint.getUrl(), user);
        });
    }

    private void post(String tenant, String url, User user) {
        try {
            UserClient client = this.clientFactory.userClient(url);
            Ack ack = client.post(user);
            user.setExternalId(ack.getExternalId());

            this.userRepository.save(user);
            this.auditService.ok(tenant, ack);
        } catch (ClientException exception) {
            this.auditService.error(tenant, exception);
        } catch (Exception exception) {
            this.auditService.error(tenant, exception);
        }
    }
}
