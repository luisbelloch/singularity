package es.luisbelloch.singularity.users;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final UserRepository repository;

    public UserController(UserRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/v1/{tenant}/users")
    public Page<User> list(@PathVariable("tenant") String tenant, Pageable page) {
        return repository.findByTenant(tenant, page);
    }
}
