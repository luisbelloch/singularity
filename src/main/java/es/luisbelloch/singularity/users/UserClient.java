package es.luisbelloch.singularity.users;

import es.luisbelloch.singularity.shared.Ack;
import feign.Headers;
import feign.RequestLine;

public interface UserClient {

    @RequestLine("POST /{functionName}")
    @Headers("Content-Type: application/json")
    Ack post(User user);
}
