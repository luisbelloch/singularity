package es.luisbelloch.singularity.users;

import es.luisbelloch.singularity.shared.EntityId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, EntityId> {

    Page<User> findByTenant(String tenant, Pageable pageable);

    Optional<User> findFirstByTenantAndExternalIdIsNullOrderByIdAsc(String tenant);

    @Query(value = "select u.id from user u where u.tenant = ?1 and u.external_id is not null order by rand() limit 1", nativeQuery = true)
    String findRandomEmittedUser(String tenant);

    @Modifying
    @Query("update User u set u.externalId = null where u.tenant = :tenant")
    void reset(String tenant);
}
