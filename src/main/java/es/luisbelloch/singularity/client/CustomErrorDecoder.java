package es.luisbelloch.singularity.client;

import feign.Response;
import feign.codec.ErrorDecoder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class CustomErrorDecoder implements ErrorDecoder {

    @Override
    public ClientException decode(String methodKey, Response response) {
        String result = getBody(response.body());
        return new ClientException(result, response.status());
    }

    private String getBody(Response.Body body) {
        try {
            return new BufferedReader(new InputStreamReader(body.asInputStream()))
                    .lines().collect(Collectors.joining("\n"));
        } catch (Exception e) {
            return "(error decoding body): " + e.toString();
        }
    }
}
