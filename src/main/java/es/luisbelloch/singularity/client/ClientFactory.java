package es.luisbelloch.singularity.client;

import es.luisbelloch.singularity.events.EventClient;
import es.luisbelloch.singularity.resources.ResourceClient;
import es.luisbelloch.singularity.users.UserClient;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.stereotype.Service;

@Service
public class ClientFactory {

    public UserClient userClient(String url) {
        return Feign.builder()
                .errorDecoder(new CustomErrorDecoder())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(UserClient.class, url);
    }

    public EventClient eventClient(String url) {
        return Feign.builder()
                .errorDecoder(new CustomErrorDecoder())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(EventClient.class, url);
    }

    public ResourceClient resourceClient(String url) {
        return Feign.builder()
                .errorDecoder(new CustomErrorDecoder())
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .target(ResourceClient.class, url);
    }
}
