package es.luisbelloch.singularity.audit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import es.luisbelloch.singularity.client.ClientException;
import es.luisbelloch.singularity.shared.Ack;

public class AuditService {
    private final String service;
    private final AuditRepository auditRepository;
    private final ObjectMapper objectMapper;

    public AuditService(String service, AuditRepository auditRepository, ObjectMapper objectMapper) {
        this.service = service;
        this.auditRepository = auditRepository;
        this.objectMapper = objectMapper;
    }

    public void ok(String tenant, Ack ack) {
        AuditEntry entry = new AuditEntry(tenant, service, "OK", serializeAck(ack));
        this.auditRepository.save(entry);
    }

    public void error(String tenant, ClientException exception) {
        AuditEntry entry = new AuditEntry(tenant, service, String.valueOf(exception.getStatus()), exception.getMessage());
        this.auditRepository.save(entry);
    }

    public void error(String tenant, Exception exception) {
        AuditEntry entry = new AuditEntry(tenant, service, "ERROR", exception.getMessage());
        this.auditRepository.save(entry);
    }

    private String serializeAck(Ack ack) {
        try {
            return objectMapper.writeValueAsString(ack);
        } catch (JsonProcessingException e) {
            return ack.getExternalId();
        }
    }
}
