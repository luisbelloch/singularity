package es.luisbelloch.singularity.audit;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class AuditView {

    private final AuditRepository repository;

    public AuditView(AuditRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/endpoints/{tenant}/{service}/logs")
    public String index(@PathVariable("tenant") String tenant, @PathVariable("service") String service, Pageable pageable, Model model) {
        Page<AuditEntry> page = this.repository.findByTenantAndService(tenant, service, pageable);

        model.addAttribute("tenant", tenant);
        model.addAttribute("service", service);
        model.addAttribute("page", page);

//        page.previousPageable().getPageNumber()

        return "audit";
    }
}
