package es.luisbelloch.singularity.audit;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Entity
@Table(name = "audit", indexes = {@Index(name = "idx_audit_entries", columnList = "tenant,service,timestamp")})
@SuppressWarnings("JpaDataSourceORMInspection")
public class AuditEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private BigInteger id;

    @Column(nullable = false)
    private String tenant;

    @Column(nullable = false)
    private String service;

    @Column(nullable = false)
    private Date timestamp;

    private String code;

    @Column(columnDefinition = "TEXT")
    private String body;

    public AuditEntry() {
    }

    public AuditEntry(String tenant, String service, String code, String body) {
        this.tenant = tenant;
        this.service = service;
        this.timestamp = new Date();
        this.code = code;
        this.body = body;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
