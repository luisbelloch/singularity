package es.luisbelloch.singularity.audit;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AuditRepository extends PagingAndSortingRepository<AuditEntry, Long> {

    Page<AuditEntry> findByTenantAndService(String tenant, String service, Pageable pageable);

    @Modifying
    @Query("delete from AuditEntry e where e.tenant = :tenant")
    void clear(String tenant);
}
