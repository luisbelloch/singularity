package es.luisbelloch.singularity.audit;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuditServiceFactory {

    @Bean(name = "user")
    public AuditService userAuditService(AuditRepository auditRepository, ObjectMapper objectMapper) {
        return new AuditService("user", auditRepository, objectMapper);
    }

    @Bean(name = "resource")
    public AuditService resourceAuditService(AuditRepository auditRepository, ObjectMapper objectMapper) {
        return new AuditService("resource", auditRepository, objectMapper);
    }

    @Bean(name = "event")
    public AuditService eventAuditService(AuditRepository auditRepository, ObjectMapper objectMapper) {
        return new AuditService("event", auditRepository, objectMapper);
    }
}
