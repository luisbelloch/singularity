package es.luisbelloch.singularity.endpoints;

import es.luisbelloch.singularity.shared.EntityId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EndpointRepository extends CrudRepository<Endpoint, EntityId> {

    List<Endpoint> findByTenant(String tenant);

    @Modifying
    @Query("update Endpoint e set e.active = false where e.tenant = :tenant")
    void disableAll(String tenant);
}
