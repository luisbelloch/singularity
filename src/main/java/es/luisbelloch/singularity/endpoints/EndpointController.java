package es.luisbelloch.singularity.endpoints;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.ResponseEntity.of;

@RestController
public class EndpointController {
    private final EndpointService service;

    public EndpointController(EndpointService service) {
        this.service = service;
    }

    @GetMapping("/v1/{tenant}/endpoints")
    public List<Endpoint> list(@PathVariable("tenant") String tenant) {
        return service.list(tenant);
    }

    @PostMapping("/v1/{tenant}/endpoints/{id}/activate")
    public ResponseEntity<Endpoint> activate(@PathVariable("tenant") String tenant, @PathVariable("id") String id) {
        return of(this.service.activate(tenant, id, true));
    }

    @PostMapping("/v1/{tenant}/endpoints/{id}/deactivate")
    public ResponseEntity<Endpoint> deactivate(@PathVariable("tenant") String tenant, @PathVariable("id") String id) {
        return of(this.service.activate(tenant, id, false));
    }

    @PostMapping("/v1/{tenant}/endpoints/{id}/url")
    public ResponseEntity<Endpoint> changeUrl(@PathVariable("tenant") String tenant, @PathVariable("id") String id, @RequestBody String url) {
        return of(this.service.changeUrl(tenant, id, url));
    }
}
