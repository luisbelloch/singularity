package es.luisbelloch.singularity.endpoints;

import es.luisbelloch.singularity.shared.EntityId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EndpointService {
    private final EndpointRepository repository;

    public EndpointService(EndpointRepository repository) {
        this.repository = repository;
    }

    public Optional<Endpoint> find(String tenant, String id) {
        return this.repository.findById(new EntityId(tenant, id));
    }

    public List<Endpoint> list(String tenant) {
        return repository.findByTenant(tenant);
    }

    public Optional<Endpoint> activate(String tenant, String id, boolean active) {
        return this.repository.findById(new EntityId(tenant, id)).map(endpoint -> {
            endpoint.setActive(active);
            return this.repository.save(endpoint);
        });
    }

    public Optional<Endpoint> changeUrl(String tenant, String id, String url) {
        return this.repository.findById(new EntityId(tenant, id)).map(endpoint -> {
            endpoint.setUrl(url);
            return this.repository.save(endpoint);
        });
    }
}
