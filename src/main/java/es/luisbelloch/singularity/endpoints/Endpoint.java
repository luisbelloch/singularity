package es.luisbelloch.singularity.endpoints;

import es.luisbelloch.singularity.shared.EntityId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(EntityId.class)
public class Endpoint {
    @Id
    private String tenant;

    @Id
    private String id;

    @Column(nullable = false)
    private String url;

    @Column(nullable = false)
    private Boolean active;

    public Endpoint() {
    }

    public Endpoint(String tenant, String id, String url, Boolean active) {
        this.tenant = tenant;
        this.id = id;
        this.url = url;
        this.active = active;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
