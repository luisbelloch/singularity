package es.luisbelloch.singularity.endpoints;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class EndpointView {

    private final EndpointService service;

    public EndpointView(EndpointService service) {
        this.service = service;
    }

    @GetMapping("/endpoints/{tenant}")
    public String index(@PathVariable("tenant") String tenant, Model model) {
        List<Endpoint> endpoints = this.service.list(tenant);

        model.addAttribute("tenant", tenant);
        model.addAttribute("endpoints", endpoints);

        return "endpoints";
    }
}
