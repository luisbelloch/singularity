package es.luisbelloch.singularity.events;

import es.luisbelloch.singularity.shared.Ack;
import feign.Headers;
import feign.RequestLine;

public interface EventClient {

    @RequestLine("POST /{functionName}")
    @Headers("Content-Type: application/json")
    Ack post(Event event);
}
