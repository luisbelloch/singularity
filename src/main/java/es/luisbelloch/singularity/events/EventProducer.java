package es.luisbelloch.singularity.events;

import es.luisbelloch.singularity.audit.AuditService;
import es.luisbelloch.singularity.client.ClientException;
import es.luisbelloch.singularity.client.ClientFactory;
import es.luisbelloch.singularity.endpoints.EndpointService;
import es.luisbelloch.singularity.resources.ResourceProducer;
import es.luisbelloch.singularity.shared.Ack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EventProducer {

    private static final Logger log = LoggerFactory.getLogger(ResourceProducer.class);

    private final EndpointService endpointService;
    private final EventGenerator eventGenerator;
    private final EventRepository eventRepository;
    private final ClientFactory clientFactory;
    private final AuditService auditService;

    public EventProducer(EndpointService endpointService, EventGenerator eventGenerator, EventRepository eventRepository,
                         ClientFactory clientFactory, @Qualifier("event") AuditService auditService) {
        this.endpointService = endpointService;
        this.eventGenerator = eventGenerator;
        this.eventRepository = eventRepository;
        this.clientFactory = clientFactory;
        this.auditService = auditService;
    }

    public void produce(String tenant) {
        endpointService.find(tenant, "event").ifPresent(endpoint -> {
            if (!endpoint.isActive()) {
                return;
            }

            Optional<Event> next = this.eventGenerator.generate(tenant);
            if (next.isEmpty()) {
                log.info("EVENT|{}|No resource or user left", tenant);
                this.endpointService.activate(tenant, "event", false);
                return;
            }

            Event event = next.get();
            post(tenant, endpoint.getUrl(), event);
        });
    }

    private void post(String tenant, String url, Event event) {
        try {
            EventClient client = this.clientFactory.eventClient(url);
            Ack ack = client.post(event);
            event.setExternalId(ack.getExternalId());

            this.eventRepository.save(event);
            this.auditService.ok(tenant, ack);
        } catch (ClientException exception) {
            this.auditService.error(tenant, exception);
        } catch (Exception exception) {
            this.auditService.error(tenant, exception);
        }
    }
}
