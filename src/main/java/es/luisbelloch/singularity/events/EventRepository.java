package es.luisbelloch.singularity.events;

import es.luisbelloch.singularity.shared.EntityId;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EventRepository extends PagingAndSortingRepository<Event, EntityId> {

    @Modifying
    @Query("delete from Event e where e.tenantId = :tenantId")
    void clear(String tenantId);
}
