package es.luisbelloch.singularity.events;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class EventJob {
    private final String[] tenants;

    private final EventProducer producer;

    public EventJob(EventProducer producer, @Value("${singularity.tenants}") String[] tenants) {
        this.producer = producer;
        this.tenants = tenants;
    }

    @Scheduled(fixedRate = 1000)
    public void run() {
        for (String tenant : tenants) {
            this.producer.produce(tenant);
        }
    }
}
