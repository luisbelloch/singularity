package es.luisbelloch.singularity.events;

import es.luisbelloch.singularity.resources.ResourceRepository;
import es.luisbelloch.singularity.users.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class EventGenerator {

    private final ResourceRepository resourceRepository;
    private final UserRepository userRepository;
    private final String[] countryCodes;

    public EventGenerator(ResourceRepository resourceRepository, UserRepository userRepository, @Value("${singularity.countries}") String[] countryCodes) {
        this.resourceRepository = resourceRepository;
        this.userRepository = userRepository;
        this.countryCodes = countryCodes;
    }

    public Optional<Event> generate(String tenant) {
        String resourceId = this.resourceRepository.findRandomEmittedResource(tenant);
        if (resourceId == null) {
            return Optional.empty();
        }

        String userId = this.userRepository.findRandomEmittedUser(tenant);
        if (userId == null) {
            return Optional.empty();
        }

        Event event = new Event();
        event.setTenantId(tenant);
        event.setEventId(UUID.randomUUID().toString());
        event.setResourceId(resourceId);
        event.setUserId(userId);

        ThreadLocalRandom random = ThreadLocalRandom.current();
        int timeDelta = random.nextInt(1, 15);
        int timeDeltaCross = random.nextInt(5, 7);

        Instant now = Instant.now();
        now.atOffset()
        event.setEventTime(now);
        event.setProcessTime(now.plus((long) timeDelta * timeDeltaCross, ChronoUnit.SECONDS));

        int countryIndex = random.nextInt(0, countryCodes.length - 1);
        event.setCountryCode(countryCodes[countryIndex]);

        Integer duration = random.nextInt(10, 60 * 5);
        event.setDuration(duration);

        if (!tenant.equals("netflix") && !tenant.equals("spotify")) {
            double itemPrice = (double) Math.round(random.nextDouble(10, 50) * 100) / 100;
            event.setItemPrice(itemPrice);
        }

        return Optional.of(event);
    }
}
