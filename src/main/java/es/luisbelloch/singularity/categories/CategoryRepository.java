package es.luisbelloch.singularity.categories;

import es.luisbelloch.singularity.shared.EntityId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CategoryRepository extends PagingAndSortingRepository<Category, EntityId> {

    Page<Category> findByTenant(String tenant, Pageable pageable);
}
