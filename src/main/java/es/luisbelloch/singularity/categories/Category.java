package es.luisbelloch.singularity.categories;

import es.luisbelloch.singularity.shared.EntityId;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(EntityId.class)
public class Category {
    @Id
    private String tenant;

    @Id
    private String id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Double percent;

    public Category() {
    }

    public Category(String tenant, String id, String name, Double percent) {
        this.tenant = tenant;
        this.id = id;
        this.name = name;
        this.percent = percent;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }
}
