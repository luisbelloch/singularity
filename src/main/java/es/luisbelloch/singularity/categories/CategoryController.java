package es.luisbelloch.singularity.categories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {
    private final CategoryRepository repository;

    public CategoryController(CategoryRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/v1/{tenant}/categories")
    public Page<Category> list(@PathVariable("tenant") String tenant, Pageable page) {
        return repository.findByTenant(tenant, page);
    }
}


