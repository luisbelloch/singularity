FROM adoptopenjdk/openjdk11:alpine-slim as assembler
WORKDIR /usr/local/src/singularity
COPY target/*.jar singularity.jar
RUN java -Djarmode=layertools -jar singularity.jar extract

FROM adoptopenjdk/openjdk11:alpine-slim
WORKDIR /usr/local/opt/singularity
ARG SOURCES=/usr/local/src/singularity
COPY --from=assembler ${SOURCES}/dependencies/ ./
COPY --from=assembler ${SOURCES}/spring-boot-loader/ ./
COPY --from=assembler ${SOURCES}/snapshot-dependencies/ ./
COPY --from=assembler ${SOURCES}/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]