DOCKER_IMAGE:=luisbelloch/singularity
COURSE_VERSION:=2020.12
GIT_COMMIT:=$(shell git rev-parse --short HEAD)
BUILD_WORKDIR:=/usr/local/src/$(DOCKER_IMAGE)
MAVEN_CMD:=mvn clean package -T 1C -DskipTests --no-transfer-progress
MAVEN_DOCKER_OPTS:=-w $(BUILD_WORKDIR) -v $(CURDIR):$(BUILD_WORKDIR) -v ~/.m2:/root/.m2
GCLOUD_PROJECT:=bigdataupv-2020
GCLOUD_IMAGE:=eu.gcr.io/${GCLOUD_PROJECT}/${DOCKER_IMAGE}
GCLOUD_REGION:=europe-west1

.PHONY: all
all: local_build container_build tag push

.PHONY: local_build
local_build:
	${MAVEN_CMD}

.PHONY: build
build:
	docker run $(MAVEN_DOCKER_OPTS) maven:3-adoptopenjdk-11 ${MAVEN_CMD} -B

container_build:
	docker build -t ${DOCKER_IMAGE}:${GIT_COMMIT} .

.PHONY: tag
tag:
	docker tag ${DOCKER_IMAGE}:${GIT_COMMIT} ${DOCKER_IMAGE}:${COURSE_VERSION}
	docker tag ${DOCKER_IMAGE}:${GIT_COMMIT} ${DOCKER_IMAGE}
	docker tag ${DOCKER_IMAGE}:${GIT_COMMIT} ${GCLOUD_IMAGE}

.PHONY: push
push:
	docker push $(GCLOUD_IMAGE)

.PHONY: cloud_build
cloud_build:
	gcloud builds submit --tag ${GCLOUD_IMAGE}

.PHONY: deploy
deploy:
	gcloud run deploy singularity --image ${GCLOUD_IMAGE} \
		--platform managed --region ${GCLOUD_REGION} \
		--memory 2Gi \
		--allow-unauthenticated

.PHONY: wiremock
wiremock:
	docker run -ti --rm -p 8090:8080 \
		-v ${PWD}/src/test/resources/wiremock:/home/wiremock \
		rodolpheche/wiremock  --verbose --global-response-templating
