terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 2.0"
    }
  }
}

provider "cloudflare" {
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

resource "cloudflare_record" "singularity" {
  zone_id = var.cloudflare_zone_id
  name    = "singularity.luisbelloch.es"
  value   = "ghs.googlehosted.com"
  type    = "CNAME"
  proxied = false
}
