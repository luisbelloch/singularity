provider "google" {
  project = var.gcloud_project
  region  = "europe-west1"
}

resource "google_cloud_run_service" "singularity" {
  name     = "singularity"
  location = "europe-west1"

  metadata {
    namespace = var.gcloud_project
  }

  template {
    spec {
      containers {
        image = "eu.gcr.io/${var.gcloud_project}/luisbelloch/singularity"
        resources {
          limits = {
            cpu    = "1000m"
            memory = "2Gi"
          }
        }
      }
    }
  }
}

data "google_iam_policy" "noauth" {
  binding {
    role = "roles/run.invoker"
    members = [
      "allUsers",
    ]
  }
}

resource "google_cloud_run_service_iam_policy" "noauth" {
  location = google_cloud_run_service.singularity.location
  project  = google_cloud_run_service.singularity.project
  service  = google_cloud_run_service.singularity.name

  policy_data = data.google_iam_policy.noauth.policy_data
}

resource "google_cloud_run_domain_mapping" "singularity" {
  location = "europe-west1"
  name     = "singularity.luisbelloch.es"

  metadata {
    namespace = var.gcloud_project
  }

  spec {
    route_name = google_cloud_run_service.singularity.name
  }
}
