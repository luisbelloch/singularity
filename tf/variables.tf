variable "cloudflare_zone_id" {}
variable "cloudflare_email" {}
variable "cloudflare_api_key" {}
variable "cloudflare_account_id" {}
variable "gcloud_project" {}
